package com.example.csvdemo.controller;


import com.example.csvdemo.service.CsvToJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/*******************************************************************************
 * Copyright (c) 2019, 2014 Mu-Sigma Business Solutions Private Limited
 * This program and the accompanying materials
 * are made available under the terms of the <<<License_Name>>>
 * which accompanies this distribution, and is available at
 * <<<Liese URL>>>
 *
 * <<< License Identifier >>>
 *
 * Contributors:
 *     Vishnu Raj - initial API and implementation
 *******************************************************************************/

@RestController
public class CsvToJsonController {

    @Autowired
    CsvToJson csvToJson;

    @RequestMapping("/csvToJson")
    public List<Map<?,?> > csvToJson() {
        return csvToJson.converToJson();
    }
}
