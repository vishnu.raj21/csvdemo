package com.example.csvdemo.service;


import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/*******************************************************************************
 * Copyright (c) 2019, 2014 Mu-Sigma Business Solutions Private Limited
 * This program and the accompanying materials
 * are made available under the terms of the <<<License_Name>>>
 * which accompanies this distribution, and is available at
 * <<<License URL>>>
 *
 * <<< License Identifier >>>
 *
 * Contributors:
 *     Vishnu Raj - initial API and implementation
 *******************************************************************************/

@Service
public class CsvToJson {

    public List<Map<?, ?>> converToJson() {
        File input = new File("/home/vishnu/Documents/DataStudio/csvdemo/src/main/resources/iris.csv");
        File output = new File("/home/vishnu/Documents/DataStudio/csvdemo/src/main/resources/iris.json");

        try {
            List<Map<?, ?>> data = readObjectsFromCsv(input);
            writeAsJson(data, output);
            return data;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<Map<?, ?>> readObjectsFromCsv(File file) throws IOException {
        CsvSchema bootstrap = CsvSchema.emptySchema().withHeader();
        CsvMapper csvMapper = new CsvMapper();
        MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(file);

        return mappingIterator.readAll();
    }

    public static void writeAsJson(List<Map<?, ?>> data, File file) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(file, data);
    }
}
